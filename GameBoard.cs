﻿using System.Collections.Generic;
using System;

namespace Kakuro.Logic
{
    public class GameBoard
    {
        List<Cell> _board;
        /// <summary>
        /// Seznam uchovavajici desku jako instance tridy Cell.
        /// </summary>
        public List<Cell> Board
        {
            get { return _board; }
            set { _board = value; }
        }
        /// <summary>
        /// Struktura uchovavajici rozmery desky.
        /// </summary>
        public BoardSize BoardSize { get; set; }

        public Difficulty Difficulty { get; set; }


        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="boardSize">Struktura udavajici velikost hraci desky.</param>
        /// <param name="difficulty">Obtiznost, pro kterou ma byt deska generovana.</param>
        public GameBoard(BoardSize boardSize, Difficulty difficulty)
        {
            // inicializace vlastnosti
            BoardSize = boardSize;
            Difficulty = difficulty;
            _board = new List<Cell>();

            // naplneni cele desky do pocatecniho stavu
            for (int i = 0; i < boardSize.height; i++)
            {
                for (int y = 0; y < boardSize.width; y++)
                {
                    // vytvoreni okraju
                    if ((i == 0) || (y % boardSize.width == 0))
                    {
                        _board.Add(new Cell { CellType = CellType.Summary });
                        continue;
                    }
                    // vytvoreni plochy
                    _board.Add(new Cell { CellType = CellType.Content });
                }
            }
        }


        /// <summary>
        /// Staticky Random, aby generoval pokazde rozdilne hodnoty.
        /// </summary>
        private static Random rnd = new Random();
        /// <summary>
        /// Genereovani desky.
        /// </summary>
        /// <param name="boardSize">Struktura s rozmery desky.</param>
        /// <param name="difficulty">Obtiznost generovane desky.</param>
        public void GenerateBoard(BoardSize boardSize, Difficulty difficulty)
        {
            GameBoard board = this;

            int fnc = 0;

            switch (fnc)
            {
                case 0:
                    GenerateBoardMirroredHorizontaly((int)difficulty, board);
                    break;
                case 1:
                    GenerateBoardMirroredVerticaly((int)difficulty);
                    break;
            }

            bool isValid = Controller.FloodFill(this);
        }

        private void GenerateBoardMirroredHorizontaly(int maxCellCount, GameBoard gameBoard)
        {
            GameBoard build = new GameBoard(gameBoard.BoardSize, gameBoard.Difficulty);
            int width = build.BoardSize.width;
            int height = build.BoardSize.height;
            int halfIndex = ((width * height) / 2) + width;
            int count = 0, counter = 0;

            for (int i = width; i < halfIndex; i++)
            {
                if (count >= maxCellCount)
                {
                    int removed = CheckHorizontalCenter();

                    if (removed == 0)
                        break;

                    if (counter == 5)
                    {
                        build = new GameBoard(gameBoard.BoardSize, gameBoard.Difficulty);
                        counter = 0;
                        count = 0;
                        i = width - 1;
                        continue;
                    }

                    counter++;
                    count -= removed;
                }

                if (Controller.CanPlaceCell(i, build))
                {
                    CellType cellType = (CellType)rnd.Next(3);
                    if (cellType == CellType.Summary)
                    {
                        build.Board.RemoveAt(i);
                        build._board.Insert(i, new Cell(cellType));
                        count++;
                    }
                }
                if ((i + 1 == halfIndex) && count < maxCellCount)
                {
                    i = width;
                }
            }

            this.Board = build.Board;

            MirrorHorizontaly(0, (width * height) / 2);
            CorrectBoard();
        }

        private void GenerateBoardMirroredVerticaly(int maxCellCount)
        {
            int width = BoardSize.width;
            int height = BoardSize.height;
            int count = 0;
            int halfIndex = BoardSize.width / 2;

            for (int i = width; i < (width * height); i++)
            {
                if (count >= maxCellCount)
                {
                    int removed = CheckHorizontalCenter();
                    if (removed == 0)
                        break;
                    count -= removed;
                }

                int target = i + halfIndex;

                if (i <= target)
                {
                    if (Controller.CanPlaceCell(width + i, this))
                    {
                        CellType cellType = (CellType)rnd.Next(3);
                        if (cellType == CellType.Summary)
                        {
                            _board.RemoveAt(width + i);
                            _board.Insert(width + i, new Cell(cellType));
                            count++;
                        }
                    }
                    if ((i + 1 == (width * height) / 2) && count < maxCellCount)
                    {
                        i = 0;
                    }
                }
                else
                    i = ((i / width) * width) + width;
            }

            MirrorVerticaly(width + 1, width * height);
            CorrectBoard();
        }


        void MirrorHorizontaly(int startIndex, int endIndex)
        {
            int halfIndex = BoardSize.height / 2;

            for (int i = startIndex; i < endIndex; i++)
            {
                if (!IsBorder(i) && Controller.IsSummaryCell(i, _board))
                {
                    int target = ((halfIndex + (halfIndex - (i / BoardSize.width))) * BoardSize.width) + i % BoardSize.width;
                    _board.RemoveAt(target);
                    _board.Insert(target, new Cell(CellType.Summary));
                }
            }
        }

        void MirrorVerticaly(int startIndex, int endIndex)
        {
            int halfIndex = BoardSize.width / 2;

            for (int i = startIndex; i < endIndex; i++)
            {
                int target = ((i / BoardSize.width) * BoardSize.width) + halfIndex;

                if (i < target)
                {
                    if (!IsBorder(i) && Controller.IsSummaryCell(i, _board))
                    {
                        target = (target - i) + target;
                        _board.RemoveAt(target);
                        _board.Insert(target, new Cell(CellType.Summary));
                    }
                }
                else
                    i = ((i / BoardSize.width) * BoardSize.width) + BoardSize.width;
            }
        }

        void CorrectBoard()
        {
            for (int i = 0; i < BoardSize.height * BoardSize.width; i++)
            {
                if (CanPlaceEmptyCell(i))
                {
                    _board.RemoveAt(i);
                    _board.Insert(i, new Cell(CellType.Empty));
                }
            }
        }

        int CheckHorizontalCenter()
        {
            int center = (BoardSize.width * BoardSize.height) / 2;
            int res = 0;
            for(int i = center; i< (center+ BoardSize.width); i++)
            {
                if (!Controller.IsBorder(i, BoardSize) && Controller.IsContentCell(i, Board) &&
                    Controller.IsSummaryCell(i - BoardSize.width, Board))
                {
                    Board.RemoveAt(i - BoardSize.width);
                    Board.Insert(i - BoardSize.width, new Cell{cellType= CellType.Content});
                    res++;
                }
            }
            return res;
        }

        bool CanPlaceEmptyCell(int index)
        {
            int width = BoardSize.width;
            int maxIndex = width * BoardSize.height;

            if (Controller.IsSummaryCell(index, _board) &&
               ((Controller.IsOnBoard(index + 1, this) && Controller.IsSummaryCell(index + 1, _board) &&
               (Controller.IsOnBoard(index + width, this) && Controller.IsSummaryCell(index + width, _board))) ||
               ((index / width == BoardSize.height - 1) &&
               (!Controller.IsOnBoard(index + 1, this) || Controller.IsSummaryCell(index + 1, _board)))))
                return true;
            else
                return false;
        }



        bool IsSpaceUpRight(int index)
        {
            int upRight = (index + 1) - BoardSize.width;
            if (!Controller.IsSummaryCell(upRight, _board) || IsBorder(upRight))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Decide, if the cell is in border
        /// </summary>
        bool IsBorder(int index)
        {
            int width = BoardSize.width;
            return (index % width == 0 || index / width == 0) ? true : false;
        }
    }
}
