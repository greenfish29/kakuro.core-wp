﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Kakuro.Logic
{
    sealed class Generator
    {
        #region sums
        private static readonly Dictionary<int, List<List<int>>> _sums = new Dictionary<int, List<List<int>>>()
        {
            {2, new List<List<int>>() {new List<int>() {1, 2}, new List<int>() {1, 3}, new List<int>() {7, 9}, new List<int>() {8, 9}}},
            {3, new List<List<int>>() {new List<int>() {1,2,3}, new List<int>() {1, 2, 4}, new List<int>() {6,8, 9}, new List<int>() {7, 8, 9}}},
            {4, new List<List<int>>() {new List<int>() {1,2,3,4}, new List<int>() {1,2,3,5}, new List<int>() {5,7,8,9}, new List<int>() {6,7,8,9}}},
            {5, new List<List<int>>() {new List<int>() {1,2,3,4,5}, new List<int>() {1,2,3,4,6}, new List<int>() {4,6,7,8,9}, new List<int>() {5,6,7,8,9}}},
            {6, new List<List<int>>() {new List<int>() {1,2,3,4,5,6}, new List<int>() {1,2,3,4,5,7}, new List<int>() {3,5,6,7,8,9}, new List<int>() {4,5,6,7,8,9}}},
            {7, new List<List<int>>() {new List<int>() {1,2,3,4,5,6,7}, new List<int>() {1,2,3,4,5,6,8}, new List<int>() {2,4,5,6,7,8,9}, new List<int>() {3,4,5,6,7,8,9}}},
            {9, new List<List<int>>() {new List<int>() {1,2,3,4,5,6,7,8,9}}}
        };

        private static readonly Dictionary<int, List<int>> _headsOfSureSums = new Dictionary<int, List<int>>() {
            {2, new List<int>() {3,4,16,17}},
            {3, new List<int>() {6,7,23,24}},
            {4, new List<int>() {10,11,29,30}},
            {5, new List<int>() {15,16,34,35}},
            {6, new List<int>() {21,22,38,39}},
            {7, new List<int>() {28,29,41,42}},
            {9, new List<int>() {45}}
        };

        //public static readonly Dictionary<int, List<List<bool>>> _sums = new Dictionary<int, List<List<bool>>>()
        //{
        //    {2, new List<List<bool>>() {new List<bool>() {true,true,false,false,false,false,false,false,false}, new List<bool>() {true,false,true,false,false,false,false,false,false}, new List<bool>() {false,false,false,false,false,false,true,false,true}, new List<bool>() {false,false,false,false,false,false,false,true,true}}},
        //    {3, new List<List<bool>>() {new List<bool>() {true,true,true,false,false,false,false,false,false}, new List<bool>() {true,true,false,true,false,false,false,false,false}, new List<bool>() {false,false,false,false,false,true,false,true,true},new List<bool>() {false,false,false,false,false,false,true,true,true}}},
        //    {4, new List<List<bool>>() {new List<bool>() {true,true,true,true,false,false,false,false,false},new List<bool>() {true,true,true,false,true,false,false,false,false},new List<bool>() {false,false,false,false,true,false,true,true,true},new List<bool>() {false,false,false,false,false,true,true,true,true}}},
        //    {5, new List<List<bool>>() {new List<bool>() {true,true,true,true,true,false,false,false,false},new List<bool>() {true,true,true,true,false,true,false,false,false},new List<bool>() {false,false,false,true,false,true,true,true,true},new List<bool>() {false,false,false,false,true,true,true,true,true}}},
        //    {6, new List<List<bool>>() {new List<bool>() {false,false,false,false,false,false,false,false,false},new List<bool>() {false,false,false,false,false,false,false,false,false},new List<bool>() {false,false,false,false,false,false,false,false,false},new List<bool>() {false,false,false,false,false,false,false,false,false}}},
        //    {7, new List<List<bool>>() {new List<bool>() {true,true,true,true,true,true,true,false,false},new List<bool>() {true,true,true,true,true,true,false,true,false},new List<bool>() {false,true,false,true,true,true,true,true,true},new List<bool>() {false,false,true,true,true,true,true,true,true}}},
        //    {9, new List<List<bool>>() {new List<bool>() {true,true,true,true,true,true,true,true,true}}}
        //};
        #endregion

        private static readonly Generator _instance = new Generator();
        public Board Board { get; private set; }
        public List<Sum> SumsList { get; private set; }
        public List<Board> Solution { get; set; }
        private int _solutionCount = 0;
        private int _counter = 0;
        public Difficulty Difficulty { get; set; }

        public static Generator Instance { get { return _instance; } }

        public void SetBoard(Board board)
        {
            this.Board = board;
            _solutionCount = 0;
            Solution = new List<Logic.Board>();
            this.SumsList = GetSums();
        }

        public void GenerateNewBoard(int width, int height, Difficulty difficulty)
        {
            _solutionCount = 0;
            Solution = new List<Logic.Board>();

            float sureSumsCountConst = 0;

            if (_counter == 10)
            {
                _counter = 0;
                this.Board = null;
            }

            if (this.Board == null)
            {
                this.Board = new Board(width, height);
                // nahodne odstranime nekolik bunek z okraje hraci desky
                RemoveCellsFromBorder();
                // nahodne odstranime nekolik bunek z hraci desky
                RemoveCellsFromBoard();
                // postupne nahodne odstranime dlouhe soucty
                RemoveCellFromLongSums(10);
                RemoveCellFromLongSums(7);
                RemoveCellFromLongSums(3);

                // pokud deska obsahuje nepripustne dlouhe soucty, vygeneruje se nova
                if (this.Board.ContainsLongSums())
                {
                    this.Board = null;
                    GenerateNewBoard(width, height, difficulty);
                    return;
                }

                this.SumsList = GetSums();
            }
            else this.Board.ToDefault();

            switch (difficulty)
            {
                case Difficulty.Easy:
                    sureSumsCountConst = 1F;
                    break;
                case Difficulty.Medium:
                    sureSumsCountConst = 1.7F;
                    break;
                case Difficulty.Hard:
                    sureSumsCountConst = 2.3F;
                    break;
            }

            InsertSureSums(SumsList, (int)((float)SumsList.Count / sureSumsCountConst));
            FillWithRandomNumbers();
            if (SumarizeValues() == false)
            {
                this.Board = null;
                GenerateNewBoard(width, height, difficulty);
                return;
            }

            // reseni jedinecnosti desky
            // nejprve vynulujeme hodnoty v polich
            SetSureValues();

            this.Board.ClearValues();
            Solve(this.Board.Width + 1);

            if (_solutionCount != 1)
            {
                this.Board = null;
                GenerateNewBoard(width, height, difficulty);
                return;
            }

            this.Board.PlaceEmptyCells();
            this.Solution[0].PlaceEmptyCells();
        }

        #region Working Kakuro generator

        /// <summary>
        /// Odebírá náhodně buňky z okraje desky.
        /// </summary>
        /// <param name="board">Herní deska.</param>
        private void RemoveCellsFromBorder()
        {
            Random random = new Random(DateTime.Now.Millisecond);

            for (int i = this.Board.Width + 1; i < this.Board.Size; i++)
            {
                if (this.Board.IsCellOnBorder(i))
                {
                    if ((random.Next(this.Board.Size) % 2) == 0)
                    {
                        if (this.Board.CanBeCellRemoved(i))
                            this.Board.RemoveCell(i);
                    }
                }
            }
        }

        /// <summary>
        /// Odstraní náhodně buňky z desky.
        /// </summary>
        /// <param name="board">Herní deska.</param>
        private void RemoveCellsFromBoard()
        {
            Random random = new Random(DateTime.Now.Millisecond);

            // projdeme celou hraci desku, krome posledni rady
            for (int i = (this.Board.Width + 1); i < this.Board.Size - this.Board.Width; i++)
            {
                // pokud je bunka soucasti okraje, preskocime ji
                if (i % this.Board.Width == 0) i++;

                if ((random.Next(this.Board.Size) % 2) == 0)
                {
                    if (this.Board.CanBeCellRemoved(i))
                        this.Board.RemoveCell(i);
                }
            }

            // projdeme posledni radu od konce
            for (int i = this.Board.Size - 1; i > (this.Board.Size - this.Board.Width) + 1; i--)
            {
                if ((random.Next(this.Board.Size) % 2) == 0)
                {
                    if (this.Board.CanBeCellRemoved(i))
                        this.Board.RemoveCell(i);
                }
            }
        }

        /// <summary>
        /// Odstraní buňky z dlouhých součtů.
        /// </summary>
        /// <param name="board">Herní deska.</param>
        /// <param name="minSumLength">Minimální délka pro součet.</param>
        private void RemoveCellFromLongSums(int minSumLength)
        {
            Cell cell;

            for (int i = (this.Board.Width + 1); i < this.Board.Size; i++)
            {
                cell = this.Board.Items[i];

                // pokud nejde o obsahovou bunku, je zbytecne na ni aplikovat algoritmus
                if (cell.CellType != CellType.Content) continue;

                if (cell.ColSumLength >= minSumLength)
                {
                    for (int y = cell.ColSumStartIndex; y <= cell.ColSumEndIndex; y++)
                    {
                        if (this.Board.Items[y].RowSumLength >= 5)
                        {
                            if (this.Board.CanBeCellRemoved(y)) this.Board.RemoveCell(y);
                        }
                    }

                    for (int y = cell.ColSumStartIndex; y <= cell.ColSumEndIndex; y++)
                    {
                        if (this.Board.Items[y].RowSumLength >= 3)
                        {
                            if (this.Board.CanBeCellRemoved(y)) this.Board.RemoveCell(y);
                        }
                    }
                }
            }
        }

        private List<Sum> GetSums()
        {
            List<Sum> sumsList = new List<Sum>();

            // projdeme celou hraci desku
            // po radcich
            for (int i = this.Board.Width + 1; i < this.Board.Size; i++)
            {
                Cell cell = this.Board.Items[i];

                // pokud se jedna o obsahove policko
                if (cell.CellType == CellType.Content)
                {
                    // vytvorime novy soucet
                    Sum sum = new Sum();
                    // nastavime typ souctu
                    sum.Type = SumType.RowSum;
                    // nastavime index jeho hlavicky
                    sum.HeadIndex = i - 1;
                    // nastavime jeho delku
                    sum.Length = cell.RowSumLength;
                    // zjistime indexy policek, ktere do nej patri
                    List<int> indexes = new List<int>();
                    for (int y = cell.RowSumStartIndex; y <= cell.RowSumEndIndex; y++)
                    {
                        sum.Items.Add(y);
                    }
                    sumsList.Add(sum);

                    i = cell.RowSumEndIndex + 1;
                }
            }

            // po sloupcich
            for (int i = this.Board.Width + 1; i < this.Board.Size; i += this.Board.Width)
            {
                Cell cell = this.Board.Items[i];

                // pokud se jedna o obsahove policko
                if (cell.CellType == CellType.Content)
                {
                    // vytvorime novy soucet
                    Sum sum = new Sum();
                    // nastavime typ souctu
                    sum.Type = SumType.ColSum;
                    // nastavime index jeho hlavicky
                    sum.HeadIndex = i - Board.Width;
                    // nastavime jeho delku
                    sum.Length = cell.ColSumLength;
                    // zjistime indexy policek, ktere do nej patri
                    List<int> indexes = new List<int>();
                    for (int y = cell.ColSumStartIndex; y <= cell.ColSumEndIndex; y += this.Board.Width)
                    {
                        sum.Items.Add(y);
                    }

                    sumsList.Add(sum);

                    i = cell.ColSumEndIndex + this.Board.Width;
                }

                if (i / this.Board.Width == this.Board.Height - 1)
                    i += this.Board.Width;
                if (i > this.Board.Size && (i % this.Board.Width != this.Board.Width - 1))
                    i = (i % this.Board.Width) + 1;
            }

            return sumsList;
        }


        /// <summary>
        /// Zjistí, které hodnoty mohou být do pole zadány.
        /// </summary>
        private List<int> GetAllowedValues(int index, Sum sum, List<Sum> sumsList)
        {
            List<int> values = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            // projdu zadany soucet a zjistim cisla obsazena v nem a oznacim je za jiz pouzita
            foreach (int i in sum.Items)
            {
                Cell cell = this.Board.Items[i];
                if (cell.Value.HasValue)
                {
                    if (i == index)
                        return new List<int>() { cell.Value.Value };

                    values.Remove(cell.Value.Value);
                }
            }

            // ziskam kolmy soucet
            Sum normalSum = GetNormalSum(index, sum, sumsList);
            // projdu ho a odeberu jiz pouzite hodnoty
            foreach (int i in normalSum.Items)
            {
                Cell cell = this.Board.Items[i];
                if (index != i && cell.Value.HasValue)
                    values.Remove(cell.Value.Value);
            }

            return values;
        }

        private void InsertSureSums(List<Sum> sumsList, int count)
        {
            List<Sum> shuffledSums = new List<Sum>(sumsList.Shuffle<Sum>());

            for (int s = 0; s < count; s++)
            {
                Sum sum = shuffledSums[s];

                // pokud existuji jednoznacne soucty danne delky
                if (_sums.ContainsKey(sum.Length))
                {
                    // ziskame jednoznacne soucty danne delky a promichame je
                    List<List<int>> shuffledSureSums = new List<List<int>>(_sums[sum.Length].Shuffle<List<int>>());

                    foreach (List<int> values in shuffledSureSums)
                    {
                        List<int> shuffledValues = new List<int>(values.Shuffle<int>());
                        List<int> passedValues = new List<int>();
                        passedValues = Rekurze(shuffledValues, sum, passedValues);

                        if (passedValues != null && sum.Items.Count > 0)
                        {
                            for (int i = 0; i < sum.Items.Count; i++)
                            {
                                int index = sum.Items[i];
                                Cell cell = this.Board.Items[index];
                                cell.Value = passedValues[i];
                                this.Board.Items[index] = cell;
                            }

                            break;
                        }
                    }
                }
            }
        }

        private Sum GetNormalSum(int index, Sum sum, List<Sum> sumList)
        {
            foreach (Sum s in sumList)
            {
                if (s.Items.Contains(index) && !s.Equals(sum))
                    return s;
            }

            return null;
        }

        private static bool CanBeValueUsed(int value, List<int> values)
        {
            return values.Contains(value);
        }

        private List<int> Rekurze(List<int> values, Sum sum, List<int> result)
        {
            for (int i = 0; i < values.Count; i++)
            {
                if (CanBeValueUsed(values[i], GetAllowedValues(sum.Items[(sum.Length - values.Count)], sum, SumsList)))
                {
                    if (values.Count > 1)
                    {
                        List<int> newSeznam = new List<int>(values);
                        result.Add(values[i]);
                        newSeznam.RemoveAt(i);
                        return Rekurze(newSeznam, sum, result);
                    }
                    else
                    {
                        result.Add(values[i]);
                        return result;
                    }
                }
            }

            return null;
        }

        private void FillWithRandomNumbers()
        {
            // seznam vsech cisel
            IEnumerable<int> values = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            // promichame vsechny soucty
            List<Sum> sums = new List<Sum>(this.SumsList.Shuffle<Sum>());
            // projdeme je jeden po druhem
            foreach (Sum sum in sums)
            {
                // pokud soucet neni kompletne vyplneny
                if (!IsSumComplete(sum))
                {
                    foreach (int index in sum.Items)
                    {
                        if (this.Board.Items[index].Value.HasValue) continue;

                        values = values.Shuffle<int>();

                        foreach (int value in values)
                        {
                            if (CanBeValueUsed(value, GetAllowedValues(index, sum, this.SumsList)))
                            {
                                Cell cell = this.Board.Items[index];
                                cell.Value = value;
                                this.Board.Items[index] = cell;
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Otestuje, jestli všechna pole součtu jsou vyplněná.
        /// </summary>
        /// <param name="sum">Testovaný součet.</param>
        private bool IsSumComplete(Sum sum)
        {
            // pro kazde policko v souctu zjisti, jestli obsahuje hodnotu
            foreach (int index in sum.Items)
            {
                // pokud je policko prazdne, neni soucet kompletni
                if (!this.Board.Items[index].Value.HasValue)
                    return false;
            }

            return true;
        }

        private bool SumarizeValues()
        {
            foreach (Sum sum in this.SumsList)
            {
                int sumValue = 0;

                foreach (int i in sum.Items)
                {
                    if (!this.Board.Items[i].Value.HasValue)
                        return false;

                    sumValue += this.Board.Items[i].Value.Value;
                }

                Cell summaryCell = this.Board.Items[sum.HeadIndex];
                switch (sum.Type)
                {
                    case SumType.RowSum:
                        summaryCell.RowSumValue = sumValue;
                        break;
                    case SumType.ColSum:
                        summaryCell.ColSumValue = sumValue;
                        break;
                }

                this.Board.Items[sum.HeadIndex] = summaryCell;
            }

            return true;
        }

        #endregion

        #region Solver

        private bool CanInsert(int value, int index, Sum sum)
        {
            Cell cell = Board.Items[index];
            List<int> values = GetAllowedValues(index, sum, SumsList);

            // pokud se bunka nachazi v jednoznacnem souctu a ma predepsane moznosti vyplneni
            if (cell.AllowedValues != null && cell.AllowedValues.Count != 0)
            {
                // a zadana hodnota neni mezi povolenymi v jednoznacnem souctu, nelze ji vlozit
                if (!cell.AllowedValues.Contains(value)) return false;

                // zjistime puvodni delku seznamu, protoze se bude z nej odebirat
                int originalLength = values.Count;

                for (int i = 0; i < values.Count; i++)
                {
                    if (!cell.AllowedValues.Contains(values[i]))
                    {
                        values.Remove(values[i]);
                        i--;
                    }
                }
            }

            Sum crossSum = GetNormalSum(index, sum, SumsList);
            Cell head = Board.Items[sum.HeadIndex];
            Cell crossHead = Board.Items[crossSum.HeadIndex];
            int sumValue = 0;
            int crossSumValue = 0;

            foreach (int i in sum.Items)
            {
                if (Board.Items[i].Value.HasValue)
                    sumValue += Board.Items[i].Value.Value;
            }

            foreach (int i in crossSum.Items)
            {
                if (Board.Items[i].Value.HasValue)
                    crossSumValue += Board.Items[i].Value.Value;
            }


            if (HighCombination(value, sum.Length, (sum.Type == SumType.RowSum ? head.RowSumValue.Value : head.ColSumValue.Value) - sumValue) &&
                LowCombination(value, sum.Length, (sum.Type == SumType.RowSum ? head.RowSumValue.Value : head.ColSumValue.Value) - sumValue) &&
                HighCombination(value, crossSum.Length, (crossSum.Type == SumType.RowSum ? crossHead.RowSumValue.Value : crossHead.ColSumValue.Value) - crossSumValue) &&
                LowCombination(value, crossSum.Length, (crossSum.Type == SumType.RowSum ? crossHead.RowSumValue.Value : crossHead.ColSumValue.Value) - crossSumValue))
            {
                return CanBeValueUsed(value, values);
            }

            return false;
        }

        /// <summary>
        /// Projde desku a polím v jednoznačných součtech nastaví možné hodnoty vyplnění.
        /// </summary>
        public void SetSureValues()
        {
            foreach (Sum sum in SumsList)
            {
                if (!_headsOfSureSums.ContainsKey(sum.Length)) continue;

                int sumValue = 0;

                switch (sum.Type)
                {
                    case SumType.RowSum:
                        sumValue = Board.Items[sum.HeadIndex].RowSumValue.Value;
                        break;
                    case SumType.ColSum:
                        sumValue = Board.Items[sum.HeadIndex].ColSumValue.Value;
                        break;
                }

                for (int i = 0; i < _headsOfSureSums[sum.Length].Count; i++)
                {
                    if (_headsOfSureSums[sum.Length][i] == sumValue)
                    {
                        foreach (int index in sum.Items)
                        {
                            Cell cell = Board.Items[index];
                            cell.AllowedValues = Generator._sums[sum.Length][i];
                            Board.Items[index] = cell;
                        }
                    }
                }
            }
        }

        public void Solve(int index)
        {
            // pokud mame 2 reseni, nema cenu dale hledat
            if (_solutionCount > 1) return;

            Sum sum = GetSum(index, SumsList);

            // pokud dojdeme na konec, mame reseni
            if (index == Board.Items.Count)
            {
                Solution.Add((Board)Board.Clone());
                _solutionCount++;
                return;
            }
            else
            {
                if (this.Board.Items[index].CellType != CellType.Content)
                {
                    Solve(index + 1);
                    return;
                }

                for (int i = 1; i < 10; i++)
                {
                    if (CanInsert(i, index, sum))
                    {
                        Cell cell = Board.Items[index];
                        cell.Value = i;
                        Board.Items[index] = cell;
                        Sum normalSum = GetNormalSum(index, sum, SumsList);

                        sum.Length--;
                        normalSum.Length--;

                        Solve(index + 1);

                        cell.Value = null;
                        Board.Items[index] = cell;
                        sum.Length++;
                        normalSum.Length++;
                    }
                }
            }
        }

        private Sum GetSum(int index, List<Sum> sums)
        {
            foreach (Sum sum in sums)
            {
                if (sum.Items.Contains(index)) return sum;
            }

            return null;
        }

        private bool HighCombination(int value, int sumLength, int summary)
        {
            int actSummary = value;
            int counter = 1;

            for (int i = 9; i > 0; i--)
            {
                if (counter == sumLength) break;
                if (i == value) continue;

                counter++;
                actSummary += i;
            }

            return actSummary >= summary;
        }

        private bool LowCombination(int value, int sumLength, int summary)
        {
            int actSummary = value;
            int counter = 1;

            for (int i = 1; i < 10; i++)
            {
                if (counter == sumLength) break;
                if (i == value) continue;

                counter++;
                actSummary += i;
            }

            return actSummary <= summary;
        }

        #endregion
    }
}