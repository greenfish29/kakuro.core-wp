﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace Kakuro.Logic
{
    /// <summary>
    /// Staticka trida umoznujici provadet kontroly hraci desky.
    /// </summary>
    public static class Controller
    {
        #region obrys hraci desky
        /// <summary>
        /// Zjisti, jestli je deska spojita.
        /// </summary>
        /// <remarks>Jde o implementaci Flood-fill algoritmu viz. Wikipedia.</remarks>
        public static bool FloodFill(Board board)
        {
            List<Cell> items = board.Items;
            // seznam s hodnotami navstivenosti policka
            List<bool> filled = new List<bool>();
            // na zacatku nastavime vsechny pole na nenavstivene
            foreach (Cell c in items) filled.Add(false);

            // zasobnik s nenavstivenymi sousedy
            Stack<int> neighbours = new Stack<int>();

            // vlozime prvni content bunku na zasobnik
            neighbours.Push(items.IndexOf(items.First(x => x.CellType == CellType.Content)));

            // spustime pruchod vsech nenavstivenych sousedu
            while (neighbours.Count > 0)
            {
                int index = neighbours.Pop();   // odebereme prvniho nenavstiveneho souseda ze zasobniku
                filled.RemoveAt(index);         // a oznacime ho za navstiveny
                filled.Insert(index, true);

                // pole s indexy vsech sousednich poli, vcetne indexu mimo desku!!!
                int[] nghbrs = { (index - 1), (index + 1),
                                 (index - board.Width), (index + board.Width) };

                // projdeme vsechny sousedy a projdou-li podminkami, pridame je na zasobnik
                foreach (int i in nghbrs)
                {
                    // testujeme, zda je pole na desce, zda jde o content pole a zda uz nebylo navstiveno
                    if (IsOnBoard(i, board) && IsContentCell(i, items) && filled[i] == false)
                        neighbours.Push(i);
                }
            }

            // projdeme vsechny content policka a zjistime, jestli zustalo nejake nenavstivene, vratime vysledek
            for (int i = 0; i < filled.Count; i++)
                if (!filled[i] && IsContentCell(i, items))
                    return false;

            return true;
        }
        /// <summary>
        /// Zjisti, jestli je pole typu ContentType.Content.
        /// </summary>
        /// <param name="index">Index pole na hraci desce.</param>
        /// <param name="board">Hraci deska.</param>
        public static bool IsContentCell(int index, List<Cell> items)
        {
            return items[index].CellType == CellType.Content ? true : false;
        }
        /// <summary>
        /// Zjisti, jestli je pole typu ContentType.Summary.
        /// </summary>
        /// <param name="index">Index pole na hraci desce.</param>
        /// <param name="board">Hraci deska.</param>
        public static bool IsSummaryCell(int index, List<Cell> items)
        {
            return items[index].CellType == CellType.Summary ? true : false;
        }
        /// <summary>
        /// Zjisti, jestli index oznacuje pole na desce.
        /// </summary>
        /// <param name="index">Index pole.</param>
        /// <param name="gameBoard">Objekt desky.</param>
        public static bool IsOnBoard(int index, Board board)
        {
            return ((index >= 0) && (index < (board.Width * board.Height))) ? true : false;
        }
        /// <summary>
        /// Zjistí, jestli v závislosti na pravidlech hry je možné buňku odstranit.
        /// </summary>
        /// <param name="index">Index pole na hrací desce.</param>
        /// <param name="board">Hrací deska.</param>
        public static bool CanBeCellRemoved(int index, Board board)
        {
            Board boardAfterRemove = (Board)board.Clone();
            boardAfterRemove.RemoveCell(index, false);

            if (FloodFill(boardAfterRemove) &&
                IsSpaceLeft(index, board) && IsSpaceRight(index, board) && IsSpaceTop(index, board) && IsSpaceDown(index, board))
                return true;

            return false;
        }
        /// <summary>
        /// Zjistí, jestli je vlevo od buňky dostatek místa.
        /// </summary>
        /// <param name="index">Index pole na hrací desce.</param>
        /// <param name="board">Hrací deska.</param>
        private static bool IsSpaceLeft(int index, Board board)
        {
            if (board.Items[index - 1].CellType == CellType.Summary)
                return true;
            else if (board.Items[index - 2].CellType == CellType.Summary)
                return false;
            else
                return true;
        }
        /// <summary>
        /// Zjistí, jestli je nad buňkou dostatek místa.
        /// </summary>
        /// <param name="index">Index pole na hrací desce.</param>
        /// <param name="board">Hrací deska.</param>
        private static bool IsSpaceTop(int index, Board board)
        {
            if (IsOnBoard(index - board.Width, board) && board.Items[index - board.Width].CellType == CellType.Summary)
                return true;
            else if (IsOnBoard(index - (2 * board.Width), board) && board.Items[index - (2 * board.Width)].CellType == CellType.Summary)
                return false;
            else
                return true;
        }
        /// <summary>
        /// Zjistí, jestli je vpravo od buňky dostatek místa.
        /// </summary>
        /// <param name="index">Index pole na hrací desce.</param>
        /// <param name="board">Hrací deska.</param>
        private static bool IsSpaceRight(int index, Board board)
        {
            if (!IsOnBoard(index + 1, board)) return true;

            if (board.Items[index + 1].CellType != CellType.Content)
                return true;
            else if (IsOnBoard(index + 2, board))
                return (board.Items[index + 2].CellType == CellType.Content);
            else
                return false;
        }
        /// <summary>
        /// Zjistí, jestli je pod buňkou dostatek místa.
        /// </summary>
        /// <param name="index">Index pole na hrací desce.</param>
        /// <param name="board">Hrací deska.</param>
        private static bool IsSpaceDown(int index, Board board)
        {
            if (!IsOnBoard(index + board.Width, board)) return true;

            if (board.Items[index + board.Width].CellType != CellType.Content)
                return true;
            else if (IsOnBoard(index + (2 * board.Width), board))
                return (board.Items[index + (2 * board.Width)].CellType == CellType.Content);
            else
                return false;
        }
        /// <summary>
        /// Zjistí, jestli hrací deska obsahuje nepřípustně dlouhé součty.
        /// </summary>
        /// <param name="board">Hrací deska.</param>
        public static bool ContainsLongSums(Board board)
        {
            for (int i = board.Width + 1; i < board.Size; i++)
            {
                if ((board.Items[i].RowSumLength > 9) || (board.Items[i].ColSumLength > 9))
                    return true;
            }

            return false;
        }
        /// <summary>
        /// Zjistí, jestli se pole nachází na okraji hrací desky.
        /// </summary>
        /// <param name="index">Index pole na hrací desce.</param>
        /// <param name="board">Hrací deska.</param>
        public static bool IsBorder(int index, Board board)
        {
            return (index % board.Width == 1 || index % board.Width == board.Width - 1 ||
                index / board.Width == 1 || index / board.Width == board.Height - 1) ? true : false;
        }

        public static bool IsMainBorder(int index, Board board)
        {
            return (index % board.Width == 0) || (index < board.Width);
        }
        #endregion

        public static bool IsBoardFormValid(Board board)
        {
            foreach (Cell cell in board.Items)
            {
                if (cell.CellType == CellType.Content)
                {
                    if (cell.ColSumLength == 1 || cell.RowSumLength == 1)
                        return false;
                }
            }

            return FloodFill(board) && !ContainsLongSums(board);
        }
    }
}