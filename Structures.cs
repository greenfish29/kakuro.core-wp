﻿/* Structures and Enums */

using System.Collections.Generic;

namespace Kakuro.Logic
{
    #region Structures

    public struct Cell
    {
        private CellType _cellType;
        /// <summary>
        /// Typ buňky.
        /// </summary>
        public CellType CellType
        {
            get { return _cellType; }
            set { _cellType = value; }
        }

        // souřadnice začátku a konce řádkových a sloupcových součtů, do kterých buňka patří
        public int RowSumStartIndex { get; set; }
        public int RowSumEndIndex { get; set; }
        public int ColSumStartIndex { get; set; }
        public int ColSumEndIndex { get; set; }

        public int RowSumLength { get { return (CellType != CellType.Content ? 0 : (RowSumEndIndex - RowSumStartIndex + 1)); } }
        public int ColSumLength { get; set; }

        public int? Value { get; set; }

        public int? RowSumValue { get; set; }
        public int? ColSumValue { get; set; }

        public List<int> AllowedValues { get; set; }
    }

    public class Sum
    {
        public int Length { get; set; }
        public List<int> Items { get; set; }
        public int HeadIndex { get; set; }
        public SumType Type { get; set; }

        public Sum()
        {
            this.Items = new List<int>();
        }
    }

    public struct BoardSize
    {
        public BoardSize(int w, int h)
        {
            width = w;
            height = h;
        }

        public int width;
        public int height;
    }

    public class Time
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }

        public Time()
        {
            Hours = Minutes = Seconds = 0;
        }

        public Time(int hours, int minutes, int seconds)
        {
            Hours = hours;
            Minutes = minutes;
            Seconds = seconds;
        }

        public void Increment()
        {
            Seconds++;
            ValidateValues();
        }

        public override string ToString()
        {
            string result = "";

            if (Hours < 10) result += "0" + Hours.ToString();
            else result += Hours.ToString();

            result += ":";

            if (Minutes < 10) result += "0" + Minutes.ToString();
            else result += Minutes.ToString();

            result += ":";

            if (Seconds < 10) result += "0" + Seconds.ToString();
            else result += Seconds.ToString();

            return result;
        }

        private void ValidateValues()
        {
            if (Seconds > 59)
            {
                Minutes++;
                Seconds = 0;
            }
            if (Minutes > 59)
            {
                Hours++;
                Minutes = 0;
            }
        }
    }

    #endregion

    #region Enums

    public enum CellType { Empty, Summary, Content }

    public enum Difficulty { Easy, Medium, Hard }

    public enum SumType { RowSum, ColSum }

    #endregion
}
