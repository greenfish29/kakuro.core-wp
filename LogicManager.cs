﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Threading;

namespace Kakuro.Logic
{
    public sealed class LogicManager
    {
        private static readonly LogicManager _instance = new LogicManager();
        private Generator generator = Generator.Instance;
        Board _board;
        private DispatcherTimer _timer;
        public Board Board
        {
            get { return _board; }
            set { _board = value; }
        }
        public Difficulty Difficulty { get; set; }
        public Time Time { get; set; }
        public event EventHandler Tick;
        public event EventHandler GameEnded;
        //public Dictionary<int, Dictionary<Difficulty, List<BoardRepositoryData>>> _boards;
        public Board Solution { get; set; }
        private Thread _thread;
        private const int maxCountForGenerating = 20;


        public static LogicManager Instance { get{return _instance;}}

        //void _timer_Tick(object sender, System.EventArgs e)
        //{
        //    this.Time.Increment();
        //    Tick(this, null);
        //}

        #region Menu Items Commands

        public void NewGame(int width, int height, Difficulty difficulty)
        {
            generator.GenerateNewBoard(width, height, difficulty);
            this.Board = generator.Board;
            this.Difficulty = difficulty;
            this.Solution = generator.Solution[0];

            //_timer = new DispatcherTimer();
            //_timer.Interval = new System.TimeSpan(0, 0, 1);
            //_timer.Tick += new System.EventHandler(_timer_Tick);

            //this.Time = new Logic.Time();
            //_timer.Start();

            //GenerateBoardsBackground(maxCountForGenerating);
        }

        public void PauseGame()
        {
            _timer.Stop();
        }

        public void UnpauseGame()
        {
            _timer.Start();
        }

        public void SetValue(int index, int? value)
        {
            Cell cell = this.Board.Items[index];
            cell.Value = value;
            this.Board.Items[index] = cell;

            if(IsGameEnded())
                if (GameEnded != null)
                    GameEnded(this, null);
        }

        public void Solve(bool throwGameEndedEvent=false)
        {
            this.Board = this.Solution;

            if (throwGameEndedEvent && GameEnded != null)
                GameEnded(this, null);
        }

        public List<int> Check()
        {
            List<int> indexesWithDiff = new List<int>();

            for (int i = this.Board.Width + 1; i < this.Board.Items.Count; i++)
            {
                Cell cell = this.Board.Items[i];
                Cell filledCell = this.Solution.Items[i];

                if (cell.CellType == CellType.Content && cell.Value.HasValue)
                {
                    if (cell.Value.Value != filledCell.Value.Value) indexesWithDiff.Add(i);
                }
            }

            return indexesWithDiff;
        }

        #endregion

        //private void InitializeBoardsDictionary()
        //{
        //    _boards = new Dictionary<int, Dictionary<Difficulty, List<BoardRepositoryData>>>();
        //    _boards.Add(64, new Dictionary<Logic.Difficulty, List<BoardRepositoryData>>());
        //    _boards[64].Add(Logic.Difficulty.Easy, new List<BoardRepositoryData>());
        //    _boards[64].Add(Logic.Difficulty.Medium, new List<BoardRepositoryData>());
        //    _boards[64].Add(Logic.Difficulty.Hard, new List<BoardRepositoryData>());
        //}

        private bool IsGameEnded()
        {
            return CompareWithSolution();
        }

        public void GenerateSolution()
        {
            generator.SetBoard(this.Board);
            generator.Solve(generator.Board.Width + 1);
            if (generator.Solution.Count > 1 || generator.Solution.Count == 0)
                return;
            this.Solution = generator.Solution[0];
        }

        private bool CompareWithSolution()
        {
            for (int i = Board.Width + 1; i < Board.Size; i++)
            {
                if (Board.Items[i].CellType == CellType.Content)
                {
                    if (!Board.Items[i].Value.HasValue || Board.Items[i].Value.Value != Solution.Items[i].Value.Value)
                        return false;
                }
            }

            return true;
        }
    }
}
