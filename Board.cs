﻿using System.Collections.Generic;
using System;
using System.Runtime.Serialization;

namespace Kakuro.Logic
{
    [DataContract]
    public class Board
    {
        [DataMember]
        public List<Cell> Items { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        
        public int Size { get { return Width * Height; } }

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="width">Šířka hrací desky.</param>
        /// <param name="height">Výška hrací desky.</param>
        public Board(int width, int height)
        {
            Width = width;
            Height = height;
            InitializeBoard();
        }

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="width">Šířka hrací desky.</param>
        /// <param name="height">Výška hrací desky.</param>
        /// <param name="items">Seznam hracích polí.</param>
        private Board(int width, int height, List<Cell> items)
        {
            Width = width;
            Height = height;
            Items = items;
        }

        /// <summary>
        /// Vrací naklonovaný objekt.
        /// </summary>
        public object Clone()
        {
            return new Board(this.Width, this.Height, new List<Cell>(this.Items));
        }

        /// <summary>
        /// Inicializace desky, vytvoří se prázdná deska s okraji.
        /// </summary>
        private void InitializeBoard()
        {
            Items = new List<Cell>();
            Items.Add(new Cell { CellType = CellType.Empty });

            for (int i = 1; i < Size; i++)
            {
                if ((i < Width) || (i % Width == 0))
                    Items.Add(new Cell { CellType = CellType.Summary });
                else
                    Items.Add(new Cell
                    {
                        CellType = CellType.Content,
                        RowSumStartIndex = (i / Width) * Width + 1,
                        RowSumEndIndex = (i / Width) * Width + (Width - 1),
                        ColSumStartIndex = (i % Width) + Width,
                        ColSumEndIndex = (i % Width) + Size - Width,
                        ColSumLength = this.Height - 1
                    });
            }
        }

        /// <summary>
        /// Zjistí, zda se buňka nachází na okraji desky.
        /// </summary>
        /// <param name="index">Index buňky.</param>
        public bool IsCellOnBorder(int index)
        {
            return Controller.IsBorder(index, this);
        }

        /// <summary>
        /// Zjistí, zda se buňka nachází na hlavním okraji desky.
        /// </summary>
        /// <param name="index">Index buňky.</param>
        public bool IsCellOnMainBorder(int index)
        {
            return Controller.IsMainBorder(index, this);
        }

        /// <summary>
        /// Zjistí, zda je možné v závislosti na pravidlech odstranit buňku z herní desky.
        /// </summary>
        /// <param name="index">Index pole na hrací desce.</param>
        public bool CanBeCellRemoved(int index)
        {
            return Controller.CanBeCellRemoved(index, this);
        }

        /// <summary>
        /// Odstraní pole z hrací desky.
        /// </summary>
        /// <param name="index">Index pole na hrací desce.</param>
        /// <param name="requiredSumsRefresh">Určuje jestli chceme přepočítávat indexy součtů.</param>
        public void RemoveCell(int index, bool requiredSumsRefresh = true)
        {
            if (requiredSumsRefresh) RefreshSums(index);
            Cell cell = new Cell() { CellType = CellType.Summary };
            Items[index] = cell;
        }

        /// <summary>
        /// Přepočítá indexy součtů na desce.
        /// </summary>
        /// <param name="index">Index odebírané buňky.</param>
        private void RefreshSums(int index)
        {
            Cell cell = Items[index];

            // zmena radkovych souctovych souradnic
            for (int i = cell.RowSumStartIndex; i <= cell.RowSumEndIndex; i++)
            {
                Cell actual = Items[i];
                if (actual.CellType == CellType.Content)
                {
                    if (i < index) actual.RowSumEndIndex = index - 1;
                    else actual.RowSumStartIndex = index + 1;
                    Items[i] = actual;
                }
            }

            // zmena sloupcovych souctovych souradnic
            for (int i = cell.ColSumStartIndex; i <= cell.ColSumEndIndex; i += Width)
            {
                Cell actual = Items[i];
                if (actual.CellType == CellType.Content)
                {
                    if (i < index) actual.ColSumEndIndex = index - Width;
                    else actual.ColSumStartIndex = index + Width;

                    actual.ColSumLength = (actual.ColSumEndIndex - actual.ColSumStartIndex) / Width + 1;

                    Items[i] = actual;
                }
            }
        }

        /// <summary>
        /// Umístí prázdná pole kolem zadaného pole.
        /// </summary>
        /// <param name="index">Index pole na hrací desce.</param>
        private void PlaceEmptyCells(int index)
        {
            int[] neighbours = { index - 1, index + 1, index - Width, index + Width };

            foreach (int i in neighbours)
            {
                if (Controller.IsOnBoard(i, this) && Controller.IsSummaryCell(i, Items))
                {
                    if ((!Controller.IsOnBoard(i + 1, this) || Items[i + 1].CellType != CellType.Content)
                        && (!Controller.IsOnBoard(i + Width, this) || Items[i + Width].CellType != CellType.Content))
                    {
                        Items[i] = new Cell { CellType = CellType.Empty };
                    }
                }
            }
        }

        public void PlaceEmptyCells()
        {
            for (int i = Width + 1; i < Size; i++)
            {
                // upravime hraci desku tak, ze do ni umistime prazdna pole
                this.PlaceEmptyCells(i);
            }
        }

        /// <summary>
        /// Zjistí, jestli hrací deska obsahuje nepřípustně dlouhé součty.
        /// </summary>
        public bool ContainsLongSums()
        {
            return Controller.ContainsLongSums(this);
        }

        public void ClearValues()
        {
            for (int i = this.Width+1; i < this.Items.Count; i++)
            {
                Cell cell = this.Items[i];
                if (cell.CellType == CellType.Content)
                {
                    cell.Value = null;
                    this.Items[i] = cell;
                }
            }
        }

        public void ToDefault()
        {
            for (int i = 0; i < this.Size; i++)
            {
                Cell cell = this.Items[i];

                switch (cell.CellType)
                {
                    case CellType.Content:
                        //cell = new Cell();
                        //cell.CellType = CellType.Content;
                        cell.Value = null;
                        this.Items[i] = cell;
                        break;
                    case CellType.Summary:
                        cell = new Cell();
                        cell.CellType = CellType.Summary;
                        this.Items[i] = cell;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
